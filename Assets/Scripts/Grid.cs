﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public LayerMask UnwalkableMask;
    public Vector2 GridWorldSize;
    public float NodeRadius;

    Node[,] grid;
    float nodeDiameter;
    int gridSizeX, gridSizeY;
    public static Vector2 GridSize;

    void Start()
    {
        nodeDiameter = NodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(GridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(GridWorldSize.y / nodeDiameter);

        createGrid();
        GridSize = GridWorldSize;
    }

    // Create the grid using the preset values
    void createGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 bottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.forward * GridWorldSize.y / 2;

        Vector3 nodePosition = Vector3.one;
        bool walkable = false;

        // Initialize every node
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                nodePosition = bottomLeft + Vector3.right * (x * nodeDiameter + NodeRadius) + Vector3.forward * (y * nodeDiameter + NodeRadius);

                // Check if the node is available
                walkable = !(Physics.CheckSphere(nodePosition, NodeRadius, UnwalkableMask));

                grid[x, y] = new Node(walkable, nodePosition, x, y);
            }
        }
    }

    // Get surrounding nodes from a node
    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0) continue;

                int checkX = node.GridX + x;
                int checkY = node.GridY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY) neighbours.Add(grid[checkX, checkY]);
            }
        }

        return neighbours;
    }

    // Get node from world position
    public Node NodeFromPosition(Vector3 pos)
    {
        int x = Mathf.RoundToInt((gridSizeX - 1) * (pos.x + GridWorldSize.x / 2) / GridWorldSize.x);
        int y = Mathf.RoundToInt((gridSizeY - 1) * (pos.z + GridWorldSize.y / 2) / GridWorldSize.y);

        return grid[x, y];
    }

    // For testing purposes (shows a grid in Editor)
    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(GridWorldSize.x, 1, GridWorldSize.y));

        if (grid != null)
        {
            foreach (Node n in grid)
            {
                Gizmos.color = n.Walkable ? Color.white : Color.red;
                Gizmos.DrawCube(n.Position, Vector3.one * nodeDiameter * 0.5f);
            }
        }
    }
}