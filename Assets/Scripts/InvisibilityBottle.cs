﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibilityBottle : MonoBehaviour
{
    // If the player has collided with an invisibility bottle, set him invisibility effect and destroy the object
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (other.gameObject.GetComponent<PlayerMovement>().EnableInvisibility())
            {
                Destroy(gameObject);
            }
        }
    }
}