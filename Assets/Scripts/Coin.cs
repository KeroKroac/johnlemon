﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public bool IsThrowable;

    void OnTriggerEnter(Collider other)
    {
        if (IsThrowable) // If coin is a throwable coin
        {
            if (other.tag == "Player" || other.tag == "PlayerInvisible" || other.tag == "areaRunning") return;

            // If coin has collided with something (that  is not the player), make every neighbour inside a 10 meters range go to that position
            foreach (Transform enemy in GameObject.Find("Enemies").transform)
            {
                if (Vector3.Distance(transform.position, enemy.position) < 10)
                {
                    if (enemy.GetComponent<PathfindingMovement>() != null)
                    {
                        enemy.GetComponent<PathfindingMovement>().CoinAt(transform.position);
                    }
                }
            }

            // Get rid of the coin
            Destroy(gameObject);
        }
        else if (other.tag == "Player" || other.tag == "PlayerInvisible") // Coin is an scenary object and the player has collided with it
        {
            if (!other.gameObject.GetComponent<PlayerMovement>().HasCoin) // If the player doesn't have a coin, give him one and destroy this
            {
                other.gameObject.GetComponent<PlayerMovement>().HasCoin = true;
                other.gameObject.transform.Find("coin").gameObject.SetActive(true);
                Destroy(gameObject);
            }
        }
    }
}