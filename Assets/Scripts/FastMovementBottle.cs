﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastMovementBottle : MonoBehaviour
{
    // If the player has collided with a fast movement bottle, set him fast effect and destroy the object
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (other.gameObject.GetComponent<PlayerMovement>().EnableFastMovement())
            {
                Destroy(gameObject);
            }
        }
    }
}