﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowEnemiesBottle : MonoBehaviour
{
    // If the player has collided with a slow enemies bottle, give all enemies the slow effect
    void OnTriggerEnter(Collider other)
    {        
        if (other.tag == "Player" || other.tag == "PlayerInvisible")
        {
            foreach (Transform t in GameObject.Find("Enemies").transform)
            {
                if (t.GetComponent <PathfindingMovement>() != null) t.GetComponent<PathfindingMovement>().EnableSlowMovement();           
            }
            Destroy(gameObject);
        }
    }
}