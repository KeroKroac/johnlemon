﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingSpider : MonoBehaviour
{
    public float Speed = 1.0f;
    Transform Model;

    // Pathfinding
    Node rndm;
    Queue<Vector3> path;
    bool avoidingPlayer;

    void Start()
    {
        // Start the spider as idle
        path = new Queue<Vector3>();
        avoidingPlayer = false;
        Model = transform.parent;
    }

    void Update()
    {        
        if (avoidingPlayer) // Spider is avoiding the player
        {
            if (path.Count > 0)
            {
                // Move to the next path node and look at it
                Model.position = Vector3.MoveTowards(Model.position, path.Peek(), Speed * Time.deltaTime);
                Model.LookAt(path.Peek());

                // Check if has arrived to the next path node
                if (Tools.CheckSamePosition(Model.position, path.Peek())) path.Dequeue();
            }
            else // Path finished but still avoiding the player, find a new one
            {
                // Get new position inside a 5 meters range
                rndm = Pathfinding.Grid.NodeFromPosition(new Vector3(Random.Range(Model.position.x - 5, Model.position.x + 5) , 0, Random.Range(Model.position.z - 5, Model.position.z + 5)));
                
                // If the node is inaccesible or is too close to the spider, find a new one
                while (rndm.Walkable == false || Vector3.Distance(rndm.Position, Model.position) < 4)
                {
                    rndm = Pathfinding.Grid.NodeFromPosition(new Vector3(Random.Range(Model.position.x - 5, Model.position.x + 5) , 0, Random.Range(Model.position.z - 5, Model.position.z + 5)));
                }
                path = Pathfinding.FindPath(Model.position, rndm.Position);
            }
        }
        else if (path.Count > 0) // If the spider has ended the avoiding player mode but has still a path, continue walking
        {
            // Move to the next path node and look at it
            Model.position = Vector3.MoveTowards(Model.position, path.Peek(), Speed * Time.deltaTime);
            Model.LookAt(path.Peek());

            // Check if has arrived to the next path node
            if (Tools.CheckSamePosition(Model.position, path.Peek())) path.Dequeue();
        }
        else Model.GetComponent<Animator>().SetBool("IsWalking", false); // Spider is not avoiding the player and has no path, reset to idle
    }

    // If the player has collided with the spider, make the spider start avoiding him
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            avoidingPlayer = true;
            Model.GetComponent<Animator>().SetBool("IsWalking", true);
        }
    }

    // If the player has left the spider detection range, stop avoiding the player
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") avoidingPlayer = false;
    }
}