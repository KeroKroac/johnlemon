﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingMovement : MonoBehaviour
{
    public Transform Player;
    public float Speed = 1.0f;

    // Pathfinding
    public List<Transform> route;
    int nextStep = 0;
    Queue<Vector3> path;
    bool followingPlayer = false;

    // Effects
    float slowMovementTimer = 0f;
    bool isSlowMovement = false;
    float lastSpeed = 0;

    void Start()
    {
        // Check if the waypoints are set
        if (route.Count == 0)
        {
            Debug.Log(gameObject.name + " route not initialized");
            this.enabled = false;
            return;
        }

        // Get route to the first waypoint
        path = Pathfinding.FindPath(transform.position, route[nextStep].position);
    }

    void Update()
    {
        if (followingPlayer) // Enemy is chasing the player
        {
            if (path.Count > 0)
            {
                // If player is now invisible, reset to patrol mode
                if (Player.tag == "PlayerInvisible")
                {
                    followingPlayer = false;
                    path.Clear();
                    return;
                }

                // Move to the next path node and look at it
                transform.position = Vector3.MoveTowards(transform.position, path.Peek(), Speed * Time.deltaTime);
                transform.LookAt(path.Peek());

                // If player position has changed, reset path to the new player position
                if (followingPlayer && path.ToArray()[path.Count - 1] != Pathfinding.GetRealPosition(Player.position))
                {
                    path = Pathfinding.FindPath(transform.position, Player.position);
                    return;
                }

                // Check if has arrived to the next path node
                if (Tools.CheckSamePosition(transform.position, path.Peek())) path.Dequeue();
            }
            else path = Pathfinding.FindPath(transform.position, Player.position); // If the enemy just started following the player and there's no route, find it
        }
        else if (path.Count > 0) // Enemy is on patrol mode
        {
            // Move to the next path node and look at it
            transform.position = Vector3.MoveTowards(transform.position, path.Peek(), Speed * Time.deltaTime);
            transform.LookAt(path.Peek());
        
            // Check if has arrived to the next path node
            if (Tools.CheckSamePosition(transform.position, path.Peek())) path.Dequeue();
        }
        else if (path.Count == 0) // Enemy is on patrol mode and already arrived to the next waypoint
        {
            // Increase waypoint to find the next
            nextStep++;

            if (nextStep == route.Count) nextStep = 0; // Keep waypoints between 0 and route.Count - 1

            // Get the route to the new waypoint
            path = Pathfinding.FindPath(transform.position, route[nextStep].position);
        }

        // Check if enemy has slow effect
        if (isSlowMovement)
        {
            slowMovementTimer -= Time.deltaTime;
            if (slowMovementTimer <= 0) // Slow effect ended, reset speed and end effect
            {
                Speed = lastSpeed;
                isSlowMovement = false;
            }
        }
    }

    // Function to enable the slow effect
    public void EnableSlowMovement()
    {
        // In case enemy was already slowed, reset speed
        if (isSlowMovement) Speed = lastSpeed;

        // Enable slow movement effect
        slowMovementTimer = 10;
        isSlowMovement = true;
        lastSpeed = Speed;
        Speed = Speed / 2;
    }

    // Function to set path to a coin
    public void CoinAt(Vector3 pos)
    {
        followingPlayer = false;
        path = Pathfinding.FindPath(transform.position, pos);
    }

    // If the player is running and has collided with the enemy, make the enemy start following him
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "areaRunning" && other.gameObject.transform.parent.tag == "Player") followingPlayer = true;
    }

    // If the player has left the enemy detection range, stop following the player and reset the path
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "areaRunning")
        {
            followingPlayer = false;
            path.Clear();
        }
    }
}