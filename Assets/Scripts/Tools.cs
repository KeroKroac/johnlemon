﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tools
{
    public static bool CheckSamePosition(Vector3 pos1, Vector3 pos2)
    {
        return Mathf.RoundToInt(pos1.x * 10) / 10 == Mathf.RoundToInt(pos2.x * 10) / 10 && Mathf.RoundToInt(pos1.z * 10) / 10 == Mathf.RoundToInt(pos2.z * 10) / 10;
    }
}