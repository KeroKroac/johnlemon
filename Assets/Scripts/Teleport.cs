﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform respawnPoint;

    private void OnTriggerEnter(Collider any)
    {
        if(any.gameObject.CompareTag("Player"))
        {
            any.transform.position = respawnPoint.transform.position;
        }
            
    }
}
