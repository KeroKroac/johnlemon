﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderKey : MonoBehaviour
{
    public GameObject KeyPlayer;

    // If the player has collided with the spider, give him the key
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerMovement>().HasKey = true;
            KeyPlayer.SetActive(true);
            Destroy(gameObject);
        }
    }
}