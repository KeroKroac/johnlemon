﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public AudioSource AudioSource_Walk;
    public AudioSource AudioSource_Run;
    public GameObject canvas;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    bool isWalking = false;
    bool isRunning = false;
    bool isInvisible = false;
    bool isFastMovement = false;
    float invisibleTimer = 0f;
    float fastMovementTimer = 0f;
    public bool HasCoin = false;
    public bool HasKey = false;
    public GameObject Coin;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        characterMove(hasHorizontalInput, hasVerticalInput);
        playSound();

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

        // If the player has the invisible effect, update the state of the effect
        if (isInvisible)
        {
            invisibleTimer -= Time.deltaTime;
            if (invisibleTimer <= 0)
            {
                isInvisible = false;
                gameObject.tag = "Player";
                transform.Find("Invisible").gameObject.SetActive(false);
            }
        }

        // If the player has the fast movement effect, update the state of the effect
        if (isFastMovement)
        {
            fastMovementTimer -= Time.deltaTime;
            if (fastMovementTimer <= 0)
            {
                isFastMovement = false;
            }
        }

        // If the player presses C and has a coin, throw the coin
        if (Input.GetKeyDown("c") && HasCoin)
        {
            GameObject coin = Instantiate(Coin, transform.Find("coin").position, transform.Find("coin").rotation);
            transform.Find("coin").gameObject.SetActive(false);
            HasCoin = false;
        }
    }

    // Function to enable the player invisibility effect
    public bool EnableInvisibility()
    {
        if (isInvisible) return false;

        // Set effect
        invisibleTimer = 10;
        isInvisible = true;
        gameObject.tag = "PlayerInvisible";
        transform.Find("Invisible").gameObject.SetActive(true);

        return true;
    }

    // Function to enable the fast movement effect
    public bool EnableFastMovement()
    {
        if (isFastMovement) return false;

        // Set effect
        fastMovementTimer = 10;
        isFastMovement = true;

        return true;
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    void playSound()
    {
        if (isWalking)
        {
            if (!AudioSource_Walk.isPlaying)
            {
                AudioSource_Walk.Play();
            }
        }
        else
        {
            AudioSource_Walk.Stop();
        }

        if (isRunning)
        {
            if (!AudioSource_Run.isPlaying)
            {
                AudioSource_Run.Play();
            }
        }
        else
        {
            AudioSource_Run.Stop();
        }
    }

    // Set player movement type
    void characterMove(bool h, bool v)
    {
        bool isMoving = h || v;
        if (isMoving)
        {
            if (Input.GetKey(KeyCode.LeftShift)) //running
            {
                isRunning = true;
                isWalking = false;
                circleRunning();
            }
            else if (isFastMovement) //walking
            {
                isWalking = false;
                isRunning = true;
                notCircleRunning();
            }
            else //walking
            {
                isWalking = true;
                isRunning = false;
                notCircleRunning();
            }
        }
        else //stop
        {
                isWalking = false;
                isRunning = false;
                notCircleRunning();
        }
        m_Animator.SetBool("IsWalking", isWalking);
        m_Animator.SetBool("IsRunning", isRunning);
    }

    // Enable the running area
    void circleRunning()
    {
        canvas.SetActive(true);
    }

    // Disable the running area
    void notCircleRunning()
    {
        canvas.SetActive(false);
    }
}